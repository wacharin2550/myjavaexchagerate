package com.example.myjavaexchangerate;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.util.*;
import android.app.Activity;

public class MainActivity extends Activity  {
    private EditText inputTextView;
    private TextView textViewTwo;
    private TextView textViewThree;
    private TextView textViewFour;
    private TextView textViewFive;
    private TextView textViewSix;
    private TextView textViewSeven;
    private Button buttonCal;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonCal = (Button) findViewById(R.id.buttonOne);
        buttonCal.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                try {

                    inputTextView = (EditText) findViewById(R.id.inputEditText);
                    textViewTwo = (TextView) findViewById(R.id.textViewTwo);
                    textViewThree = (TextView) findViewById(R.id.textViewThree);
                    textViewFour = (TextView) findViewById(R.id.textViewFour);
                    textViewFive = (TextView) findViewById(R.id.textViewFive);
                    textViewSix = (TextView) findViewById(R.id.textViewSix);
                    textViewSeven = (TextView) findViewById(R.id.textViewSeven);
                    Log.d("notescreen", "This is the text: " + inputTextView.getText());
                    textViewTwo.setText(calCulate(inputTextView.getText().toString(),2));
                    textViewThree.setText(calCulate(inputTextView.getText().toString(),3));
                    textViewFour.setText(calCulate(inputTextView.getText().toString(),4));
                    textViewFive.setText(calCulate(inputTextView.getText().toString(),5));
                    textViewSix.setText(calCulate(inputTextView.getText().toString(),6));
                    textViewSeven.setText(calCulate(inputTextView.getText().toString(),7));
                } catch (Exception e) {
                    Log.e("MYAPP", "exception", e);
                }
            }
        });
    }
    private String calCulate(String input,int percent){
        String number = Integer.toString(Integer.parseInt(input)*percent);
        return number;
    }




}
